#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

struct ili9488_opt_t g_ili9488_display_opt;

//#define LED_PIO PIOC
//#define LED_PIO_ID ID_PIOC
//#define LED_PIO_PIN 8
//#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define LED1_PIO PIOA
#define LED1_PIO_ID ID_PIOA
#define LED1_PIO_PIN 0
#define LED1_PIO_PIN_MASK (1 << LED1_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK ( 1 << BUT_PIO_PIN)

#define BUT1_PIO PIOD
#define BUT1_PIO_ID ID_PIOD
#define BUT1_PIO_PIN 28
#define BUT1_PIO_PIN_MASK ( 1 << BUT1_PIO_PIN)

#define BUT2_PIO PIOC
#define BUT2_PIO_ID ID_PIOC
#define BUT2_PIO_PIN 31
#define BUT2_PIO_PIN_MASK ( 1 << BUT2_PIO_PIN)

#define BUT3_PIO PIOA
#define BUT3_PIO_ID ID_PIOA
#define BUT3_PIO_PIN 19
#define BUT3_PIO_PIN_MASK ( 1 << BUT3_PIO_PIN)

volatile Bool but_flag = false;
volatile Bool but1_flag = false;
volatile Bool but2_flag = false;
volatile Bool but3_flag = false;

volatile float freq = 0;

void but_callBack(void){
	but_flag = true;
}

void but1_callBack(void){
	but1_flag = true;
}

void but2_callBack(void){
	if (freq > 0){
		freq -= 1;
		but2_flag = true;
	}
}

void but3_callBack(void){
	freq += 1;
	but3_flag = true;
}

void pisca_led(float frequencia){	
	for (float i = 0; i < (2/(1/frequencia)); i++){
		pio_set(LED1_PIO, LED1_PIO_PIN_MASK);
		delay_s((1/frequencia)/2);

		pio_clear(LED1_PIO, LED1_PIO_PIN_MASK);
		delay_s((1/frequencia)/2);
	}
	pio_set(LED1_PIO, LED1_PIO_PIN_MASK);
}

void show_freq(uint32_t freq){
	uint8_t stingLCD[256];
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(10, 200, ILI9488_LCD_WIDTH-1, 215);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Frequencia: %d Hz", freq);
	ili9488_draw_string(10, 200, stingLCD);
}


void configLed(){
	// Ativa  o periferico responsavel pelo controle do LED
	pmc_enable_periph_clk(LED1_PIO_ID);
	
	pio_configure(LED1_PIO, PIO_OUTPUT_0, LED1_PIO_PIN_MASK, PIO_DEFAULT);
	
	pio_set(LED1_PIO, LED1_PIO_PIN_MASK);
	
}

void configBut(){
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	// Configura botao como entrada
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but_callBack);
	
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
}

void configBut1(){
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT1_PIO_ID);
	
	// Configura botao como entrada
	pio_configure(BUT1_PIO, PIO_INPUT, BUT1_PIO_PIN_MASK, PIO_PULLUP);
	
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but1_callBack);
	
	pio_enable_interrupt(BUT1_PIO, BUT1_PIO_PIN_MASK);
	
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 0);
}

void configBut2(){
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT2_PIO_ID);
	
	// Configura botao como entrada
	pio_configure(BUT2_PIO, PIO_INPUT, BUT2_PIO_PIN_MASK, PIO_PULLUP);
	
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but2_callBack);
	
	pio_enable_interrupt(BUT2_PIO, BUT2_PIO_PIN_MASK);
	
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 0);
}

void configBut3(){
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT3_PIO_ID);
	
	// Configura botao como entrada
	pio_configure(BUT3_PIO, PIO_INPUT, BUT3_PIO_PIN_MASK, PIO_PULLUP);
	
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but3_callBack);
	
	pio_enable_interrupt(BUT3_PIO, BUT3_PIO_PIN_MASK);
	
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 0);
}


/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

    /* Inicializa e configura o LCD */
	configure_lcd();

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
	configLed();
	configBut();
	configBut1();
	configBut2();
	configBut3();
	
	freq = 1;
	
	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		if (but_flag){
			pisca_led(freq);
			but_flag = false;
			show_freq(freq);
		}
		if (but1_flag){
			pisca_led(freq);
			but1_flag = false;
			show_freq(freq);
		}
		if (but2_flag){
			but2_flag = false;
			show_freq(freq);
		}
		if (but3_flag){
			but3_flag = false;
			show_freq(freq);
		}
	}
	return 0;
}
