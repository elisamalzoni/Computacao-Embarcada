/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

#define YEAR        2018
#define MOUNTH      4
#define DAY         9
#define WEEK        15
#define HOUR        10
#define MINUTE      45
#define SECOND      0


#define LED1_PIO PIOA
#define LED1_PIO_ID ID_PIOA
#define LED1_PIO_PIN 0
#define LED1_PIN_MASK (1 << LED1_PIO_PIN)

#define LED2_PIO PIOC
#define LED2_PIO_ID ID_PIOC
#define LED2_PIO_PIN 30
#define LED2_PIN_MASK (1 << LED2_PIO_PIN)

#define BUT1_PIO PIOD
#define BUT1_PIO_ID ID_PIOD
#define BUT1_PIO_PIN 28
#define BUT1_PIN_MASK ( 1 << BUT1_PIO_PIN)

#define BUT2_PIO PIOC
#define BUT2_PIO_ID ID_PIOC
#define BUT2_PIO_PIN 31
#define BUT2_PIN_MASK ( 1 << BUT2_PIO_PIN)

#define BUT3_PIO PIOA
#define BUT3_PIO_ID ID_PIOA
#define BUT3_PIO_PIN 19
#define BUT3_PIN_MASK ( 1 << BUT3_PIO_PIN)


#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"


void BUT1_init(void);
void BUT2_init(void);
void BUT3_init(void);
void LED1_init(int estado);

void pisca_led(float frequencia);
void atualiza_hora(void);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);

volatile uint8_t flag_led1 = 1;
volatile Bool configurando = 0;
volatile uint32_t alarme = 0;
volatile char string_hora[150];
volatile char string_alarme[10];
volatile uint32_t hora = HOUR;
volatile uint32_t minuto = MINUTE;
volatile uint32_t toggle = 0;
volatile uint32_t hora_alarme;
volatile uint32_t minuto_alarme;

void pin_toggle(Pio *pio, uint32_t mask);

volatile Bool is_toggling = 0;

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

void pisca_led(float frequencia){
	for (float i = 0; i < (2/(1/frequencia)); i++){
		pio_set(LED2_PIO, LED2_PIN_MASK);
		delay_s((1/frequencia)/2);

		pio_clear(LED2_PIO, LED2_PIN_MASK);
		delay_s((1/frequencia)/2);
	}
	pio_set(LED1_PIO, LED1_PIN_MASK);
}

void atualiza_hora(){
	if (minuto < 10){
		sprintf(string_hora, "%d:%02d", hora, minuto);
		gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
		gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
	}else{
		sprintf(string_hora, "%d:%d", hora, minuto);
		gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
		gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
	}
	if (alarme != 0){
		gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
		
		
	}
	
}

static void Button1_Handler(uint32_t id, uint32_t mask){
	configurando = !configurando;
	if (configurando){
		pio_clear(LED1_PIO, LED1_PIN_MASK);
		gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
		gfx_mono_draw_string(string_alarme, 0, 0, &sysfont);
	
	}else{
		pio_set(LED1_PIO, LED1_PIN_MASK);
		atualiza_hora();
		if (alarme != 0){
			uint32_t h, m, s;
			rtc_get_time(RTC, &h, &m, &s);
			rtc_set_time_alarm(RTC, 1, h, 1, m+alarme, 1, s);
			minuto_alarme = m+alarme;
			hora_alarme = h;
			
		}
		
	}
}

static void Button2_Handler(uint32_t id, uint32_t mask){
	if (configurando){
		alarme++;
		sprintf(string_alarme, "%d", alarme);
		gfx_mono_draw_string(string_alarme, 0, 0, &sysfont);
	}else{
		if (is_toggling){
			is_toggling = 0;
		}
		else{
			if (alarme != 0){
				if ((minuto + alarme)>59){
					hora_alarme += (minuto_alarme)/60;
					minuto_alarme = abs(60 - (minuto_alarme));
				
					if (minuto < 10){
						sprintf(string_hora, "%d:%02d", hora_alarme, minuto_alarme);
						gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
						gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
					}else{
						sprintf(string_hora, "%d:%d", hora_alarme, minuto_alarme);
						gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
						gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
					}
				}
				if (minuto < 10){
					sprintf(string_hora, "%d:%02d", hora_alarme, minuto_alarme);
					gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
					gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
				}else{
					sprintf(string_hora, "%d:%d", hora_alarme, minuto_alarme);
					gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
					gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
				}
			}else{
				gfx_mono_draw_filled_rect(0, 0, 128 , 32, GFX_PIXEL_CLR);
				gfx_mono_draw_string("s alarm", 0, 0, &sysfont);
			}
		}
		
	}
}
		

static void Button3_Handler(uint32_t id, uint32_t mask){
	if (configurando){
		if (alarme > 0){
			alarme --;
		}
		sprintf(string_alarme, "%d", alarme);
		gfx_mono_draw_string(string_alarme, 0, 0, &sysfont);
	} else{
		alarme=0;
		atualiza_hora();
	}
	
}

void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	uint32_t h, m, s;
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		rtc_get_time(RTC, &h, &m, &s);
		if (m != minuto){
			minuto = m;
			hora = h;
			atualiza_hora();
		} 
		if (toggle > 0){
			toggle--;
			pin_toggle(LED2_PIO, LED2_PIN_MASK);
		}
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		
		toggle = 10;
		alarme = 0;
		atualiza_hora();
		
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}



void BUT1_init(void){
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);

	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
}

void BUT2_init(void){
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);

	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);
}

void BUT3_init(void){
	pmc_enable_periph_clk(BUT3_PIO_ID);
	pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, Button3_Handler);

	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 1);
}

void LED1_init(int estado){
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, estado, 0, 0 );
}
void LED2_init(int estado){
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, estado, 0, 0 );
}


/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN);

}

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	
	LED1_init(1);
	LED2_init(1);
	
	BUT1_init();
	BUT2_init();
	BUT3_init();
	
	RTC_init();
	
	atualiza_hora();
	

	
	//gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
    gfx_mono_draw_string(string_hora, 0, 0, &sysfont);
	
	configurando = 0;
	alarme = 0;
	pio_set(LED1_PIO, LED1_PIN_MASK);
	
	while(1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		
	}
}

